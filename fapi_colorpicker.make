core = 7.x
api = 2

; Spectrum Colorpicker lib.

libraries:
  bgrins-spectrum:
    download:
      type: file
      url: https://github.com/bgrins/spectrum/archive/1.7.0.zip
